#include <cstddef>
#include "dllist.h"

// Initialize a list element
DLLElement::DLLElement(void *itemPtr, int sortKey)
	:item(itemPtr), key(sortKey) {
	next = NULL;
	prev = NULL;
}

// Constructor
DLList::DLList() {
	first = NULL;
	last = NULL;
}

// Destructor
DLList::~DLList() {
	DLLElement *ptr = first;

	if (ptr) {
		first = first->next;
		delete(ptr);
		ptr = first;
	}
}

// add to head of list (set key = min_key-1)
void DLList::Prepend(void *item) {
	// if the list is not empty
	if (first) {
		DLLElement *elem = new DLLElement(item, (int)(first->key) - 1);

		elem->next = first;
		first->prev = elem;

		first = elem;
	}
	// if the list is empty
	else {
		DLLElement *elem = new DLLElement(item, 0);
		first = last = elem;
	}
}

// add to tail of list (set key = max_key+1)
void DLList::Append(void *item) {
	// if the list is not empty
	if (last) {
		DLLElement *elem = new DLLElement(item, (int)(last->key) + 1);

		last->next = elem;
		elem->prev = last;

		last = elem;
	}
	// if the list is empty
	else {
		DLLElement *elem = new DLLElement(item, 100);
		first = last = elem;
	}
}

// remove from head of list
void *DLList::Remove(int *keyPtr) {
	void *item = NULL;

	// if the list is not empty
	if (first) {
		DLLElement *ptr = first;
		first = first->next;

		// if the list contains more than one element
		if (first)
			first->prev = NULL;

		*keyPtr = ptr->key;

		item = ptr->item;
		delete(ptr);
	}
	return item;
}

// return pointer to the first element of the list
DLLElement *DLList::GetFirst() {
	return first;
}

// return pointer to the last element of the list
DLLElement *DLList::GetLast() {
	return last;
}

// return true if list has no elements
bool DLList::IsEmpty() {
	return (first ? false : true);
}

// routines to put items on the list in order (sorted by key)
void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement *elem = new DLLElement(item, sortKey);

	// if the list is not empty
	if (first) {
		// if the element for insert has the lowest priority (biggest key value)
		if (last->key <= sortKey) {
			last->next = elem;
			elem->prev = last;
			last = elem;
		}
		// if priority is not the lowest
		else
			// iterating through the list
			for (DLLElement *ptr = first;;ptr = ptr->next) {
				// if a place to store element is found
				if (sortKey < ptr->key) {
					// if element is to be inserted in the middle of the list
					if (ptr->prev) {
						ptr->prev->next = elem;
						elem->prev = ptr->prev;
					}
					// if element is to be inserted in the head of the list
					else
						first = elem;

					elem->next = ptr;
					ptr->prev = elem;

					break;
				}
			}
	}
	// if the list is empty
	else
		first = last = elem;
}

// remove first item with key==sortKey
void *DLList::SortedRemove(int sortKey) {
	void *item = NULL;

	// if the list is not empty
	if (first) {
		// iterating through the list
		for (DLLElement *ptr = first; ptr; ptr = ptr->next) {
			// if searched element is found
			if (ptr->key == sortKey) {
				// if the element in the middle of the list
				if (ptr->prev && ptr->next) {
					ptr->prev->next = ptr->next;
					ptr->next->prev = ptr->prev;
				}
				// if element has pointer to previous element
				// but has no pointer to next
				else if (ptr->prev)
					ptr->prev->next = NULL;
				// if element has pointer to next element
				// but has no pointer to previous
				else if (ptr->next)
					ptr->next->prev = NULL;

				item = ptr->item;
				delete(ptr);
			}
		}
	}
	return item;
}
