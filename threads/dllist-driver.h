#include "dllist.h"

void generate(DLList *list, int n, int which); // generate N elements with random values
void remove(DLList *list, int n, int which);   // remove N elements starting from the head of the list
void SimpleTask(void **args);   //Threads testing function
void HW1Test(int T, int N);     //Function, which generates T threads
